package domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "trains")
public class Train {

    @Id
    @Column(name = "train_id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "number_train")
    private String trainNumber;
    @Column(name = "carrige_number")
    private int carrigeNumber;

    public Train() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public int getCarrigeNumber() {
        return carrigeNumber;
    }

    public void setCarrigeNumber(int carrigeNumber) {
        this.carrigeNumber = carrigeNumber;
    }
}
