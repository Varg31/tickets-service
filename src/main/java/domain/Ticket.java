package domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    int ticket_id;
    @Column(name = "price")
    double price;
    @Column(name = "number_ticket")
    String ticketNumber;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "trips_trip_id")
    private List<Trip> stations = new ArrayList<Trip>();

    public Ticket() { }

    public List getStations() {
        return stations;
    }

    public void setStations(List stations) {
        this.stations = stations;
    }

    public int getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(int ticket_id) {
        this.ticket_id = ticket_id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }







}

