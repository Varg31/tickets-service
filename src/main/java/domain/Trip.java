package domain;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Entity
@Table(name = "trips")
public class Trip {

    @Id
    @Column(name = "trip_id")           // FIXED BUG, ADD COLUMN
    private int id;

    @Column(name = "status")
    private String status;

    @Column(name = "data_start")
    private Date dataStart;

    @Column(name = "time_start")
    private Time timeStart;

    @ManyToOne
    private Train train = new Train();

    public Trip() { }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train trainList) {
        this.train = trainList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataStart() {
        return dataStart;
    }

    public void setDataStart(Date dataStart) {
        this.dataStart = dataStart;
    }

    public Time getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Time timeStart) {
        this.timeStart = timeStart;
    }


}
