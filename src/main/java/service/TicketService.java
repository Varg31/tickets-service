package service;

import domain.Ticket;
import org.hibernate.Session;

import org.hibernate.query.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TicketService {

    Ticket ticket;

    private static Session session;
    List<Ticket> ticketList;

    public TicketService() {
        ticketList = new ArrayList<Ticket>();
        ticket = new Ticket();
    }

    public List<Double> getPrice() {
        session = HibernateUtil.getSession();

        return findAll().stream()
                .map(Ticket::getPrice)
                .collect(Collectors.toList());
    }

    public List getAllObjects() {
        return findAll();
    }

    public List getStations() {
        session = HibernateUtil.getSession();

        return ticket.getStations();
    }

    private List<Ticket> findAll() {
        List<Ticket> objects = new ArrayList<Ticket>();

        Query query = session.createQuery("from Ticket");

        for(Object obj: query.list()) {
            Ticket entity = (Ticket) obj;

            objects.add(entity);
        }
        return objects;

    }
}
